$(document).ready(function() {
  ////////////////////////// left side accordian on published report description////////////////////////////////////////////////////

 
 
  ///////////////////////////
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;


		var links = this.el.find('.link');
	
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.rd_accordian_submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	

  var accordion = new Accordion($('#pubAccordion'), false);
  
////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////

    function getUrlParameter(variable) {
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
      }
      return(false);
    }


    var tester = getUrlParameter("search_query");
    console.log(tester);
    var level_1 = getUrlParameter("level_1");
    var level_2 = getUrlParameter("level_2");
    var level_3 = getUrlParameter("level_3");
    var current = getUrlParameter("cur");
    $("p." + current).addClass("current");
    $('#' + level_1 + '.collapse').collapse('show');
    setTimeout(function() {
     $('#' + level_1 + ' #' + level_2).collapse('show');
   }, 1000);
    setTimeout(function() {
     $('#' + level_1 + ' #' + level_2 + ' #' + level_3).collapse('show');
   }, 2000);



// internal tabs accounts 
 // tabbed content

 $(".tab_content").hide();
 $(".tab_content:first").show();

 /* if in tab mode */
 $("ul.tabs li").click(function() {

   $(".tab_content").hide();
   var activeTab = $(this).attr("rel"); 
   $("#"+activeTab).fadeIn();		

   $("ul.tabs li").removeClass("active");
   $(this).addClass("active");

   $(".tab_drawer_heading").removeClass("d_active");
   $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

 });
 $(".tab_container").css("min-height", function(){ 
   return $(".tabs").outerHeight() + 50;
 });

 $(".tab_drawer_heading").click(function() {

   $(".tab_content").hide();
   var d_activeTab = $(this).attr("rel"); 
   $("#"+d_activeTab).fadeIn();

   $(".tab_drawer_heading").removeClass("d_active");
   $(this).addClass("d_active");

   $("ul.tabs li").removeClass("active");
   $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
 });

//////////////////////////////////////////

// Accordian left side list ON new RD page

var Accordion = function(el, multiple) {
  this.el = el || {};
  this.multiple = multiple || false;


  var links = this.el.find('.link');

  links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
}

Accordion.prototype.dropdown = function(e) {
  var $el = e.data.el;
  $this = $(this),
  $next = $this.next();

  $next.slideToggle();
  $this.parent().toggleClass('open');

  if (!e.data.multiple) {
   $el.find('.rd_accordian_submenu').not($next).slideUp().parent().removeClass('open');
 };
}	

var accordion = new Accordion($('#rdAccordion'), false);

// //////////////////////////////

// ///// Read More Button on RD Page /////
$('.expand_read').on('click', function(){
  $('.special-text').toggleClass('-expanded');

  if ($('.special-text').hasClass('-expanded')) {
    $('.expand_read').html('Less');
  } else {
    $('.expand_read').html('Read More');
  }
});
// ///// Read More Button on RD Page end/////



// //Table On Company Profiles

$('.cp-table').find("th").each(function (i) {

  $('.cp-table td:nth-child(' + (i + 1) + ')').prepend('<span class="cp-table-thead">'+ $(this).text() + ':</span> ');
  $('.cp-table-thead').hide();
});

$( '.cp-table' ).each(function() {
  var thCount = $(this).find("th").length; 
  var rowGrow = 100 / thCount + '%';
  $(this).find("th, td").css('flex-basis', rowGrow);   
});

function flexTable(){
  if ($(window).width() < 768) {

    $(".cp-table").each(function (i) {
      $(this).find(".cp-table-thead").show();
      $(this).find('thead').hide();
    });

  } else {
    $(".cp-table").each(function (i) {
      $(this).find(".cp-table-thead").hide();
      $(this).find('thead').show();
    });

  }

}      

flexTable();
window.onresize = function(event) {
  flexTable();
};

 /////////
 
 

////New rd page////
$("#sidebar-toggle").click(function(e) {
  e.preventDefault();
  $("#rdMainWrapper").toggleClass("toggled");
});
//////////////////


// sticky sidebar new rd page
$('[data-toggle="tooltip"]').tooltip()
///////


/////////////cart checkboxes in subscription page/////////////
/////// for
$('#selectAllChapters').click(function () {
  $('.selectedChaps').prop('checked', this.checked);
});

$('.selectedChaps').change(function () {
  var check = ($('.selectedChaps').filter(":checked").length == $('.selectedChaps').length);
  $('#selectAllChapters').prop("checked", check);
});


/////////////
/////// for
$('#selectAllGeos').click(function () {
  $('.selectedgeovs').prop('checked', this.checked);
});

$('.selectedgeovs').change(function () {
  var check = ($('.selectedgeovs').filter(":checked").length == $('.selectedgeovs').length);
  $('#selectAllGeos').prop("checked", check);
});


//////////////////// for
$('#selectAllCps').click(function () {
  $('.selectedcmps').prop('checked', this.checked);
});

$('.selectedcmps').change(function () {
  var check = ($('.selectedcmps').filter(":checked").length == $('.selectedcmps').length);
  $('#selectAllCps').prop("checked", check);
});


//////////////////// for
$('#selectWholereport').click(function () {
  $('.selectedwhRrt').prop('checked', this.checked);
});

$('.selectedwhRrt').change(function () {
  var check = ($('.selectedwhRrt').filter(":checked").length == $('.selectedwhRrt').length);
  $('#selectWholereport').prop("checked", check);
});

 ////////////////////////////

  // $("#selectWholereport").click(function() 
  // { 
  //   $('.select_all').addClass('select_all_gray , card_gray_border')
  //  });
         /////////
        //  $("#selectWholereport").click(function() 
        //  { 
        //    $('.chapter_list').not(this).addClass('card_gray_border')
        //   });   
  /////////////Subscription page scroll to sectop
  $("a[href^='#scrlll']").click(function(e) {
    e.preventDefault();
    
    var position = $($(this).attr("href")).offset().top;
  
    $("body, html").animate({
      scrollTop: position
    } ,1500 );
  });
/////////////////
///////// Profile Image Upload ////////////
var readURL = function(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('.profile-img').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
  }
}

$(".file-upload").on('change', function(){
  readURL(this);
});

$(".upload-button").on('click', function() {
 $(".file-upload").click();
});
///////////////////////////

//////////// subscibe window (rd)///////////
setTimeout(function(){
  $('#subOverlay').fadeIn(1000);
}, 2000);
////////////////////////


/////////////////// payment process /////////////

"use strict";
let loading = document.querySelector(".paymant-load");
let letters = loading.textContent.split("");
loading.textContent = "";
letters.forEach((letter, i) => {
    let span = document.createElement("span");
    span.textContent = letter;
    span.style.animationDelay = `${i / 5}s`;
    loading.append(span);
});

/////////////////// payment process /////////////



}); // main bracket closed



//// /20122019/ report listing published report ////

$(".dropdown-menu li a").click(function(){
  $(this).parents(".dropdown").find('.btn').html($(this).html() + ' <span class="caret"></span>');
  $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
});

// published details page dropdown selector
$(".details_regional_dd .dropdown-menu li a").click(function(){
  $(this).parents(".dropdown").find('.btn').html($(this).html() + ' <span class="caret"></span>');
  $(".details_regional_dd button .dnone").css({"display": "none"});

  // $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
});
//////  //////


///////////////////


// ///// Data popover on published report maps 30-12-2019//////


$(document).ready(function(){
  ///
  $('.region_africa [data-toggle="popover"]').popover({
    placement : 'right',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> Africa</h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ///  
  ///
   $('.region_asia [data-toggle="popover"]').popover({
    placement : 'right',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> Asia Pacific</h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ///  
  ///
  $('.region_europe [data-toggle="popover"]').popover({
    placement : 'right',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> Europe</h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ///  
  ///
  $('.region_latin_america [data-toggle="popover"]').popover({
    placement : 'right',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> Latin America</h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ///  
  ///
  $('.region_north_america [data-toggle="popover"]').popover({
    placement : 'top',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> North America</h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ///  
  ///

  // //////////// popover for North-America Countries   //////////
  $('.country_canada [data-toggle="popover"]').popover({
    placement : 'right',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> Canada </h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ////
  $('.country_greenland [data-toggle="popover"]').popover({
    placement : 'bottom',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> Greenland </h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ////
  $('.country_mexico [data-toggle="popover"]').popover({
    placement : 'right',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> Mexico </h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ////
  $('.country_usa [data-toggle="popover"]').popover({
    placement : 'left',
    trigger : 'hover',
    html : true,
    content:'  <div class="pub_regions_pop_body"><h4 class="pub_regions_pop_heading"><span></span> USA </h4><ul class="list-unstyled"><li><span>Revenue</span><span>$ 9,123,596</span></li><li><span>CAGR</span><span>5.4 %</i></span></li></ul></div></div>'
  });
  ////
  /////////////////////////////////////////////


//Pop over End  ////////////


///////// thumbnail carousel on publishsed report ///////
$('#carousel-example').on('slide.bs.carousel', function (e) {

  var $e = $(e.relatedTarget);
  var idx = $e.index();
  var itemsPerSlide = 5;
  var totalItems = $('.carousel-item').length;

  if (idx >= totalItems-(itemsPerSlide-1)) {
    var it = itemsPerSlide - (totalItems - idx);
    for (var i=0; i<it; i++) {

      if (e.direction=="left") {
        $('.carousel-item').eq(i).appendTo('.carousel-inner');
      }
      else {
        $('.carousel-item').eq(0).appendTo('.carousel-inner');
      }
    }
  }
});
////////

});

////////// read more toggle btn on pub report ////////
$(document).ready(function() {
  $("#toggleReadBtn").click(function() {
    var elem = $("#toggleReadBtn").text();
    if (elem == "Read More") {
      //Stuff to do when btn is in the read more state
      $("#toggleReadBtn").text("Read Less");
      $("#text").slideDown();
    } else {
      //Stuff to do when btn is in the read less state
      $("#toggleReadBtn").text("Read More");
      $("#text").slideUp();
    }
  });
});
/////////

// /////// Switch Button on Published Report Page  /// /mobile view ///////
'use strict';

var switchButton 			= document.querySelector('.pub-value-switch-button');
var switchBtnRight 			= document.querySelector('.pub-value-switch-button-case.right');
var switchBtnLeft 			= document.querySelector('.pub-value-switch-button-case.left');
var activeSwitch 			= document.querySelector('.active');

function switchLeft(){
	switchBtnRight.classList.remove('switch-active-case');
	switchBtnLeft.classList.add('switch-active-case');
	activeSwitch.style.left 						= '0%';
}

function switchRight(){
	switchBtnRight.classList.add('switch-active-case');
	switchBtnLeft.classList.remove('switch-active-case');
	activeSwitch.style.left 						= '50%';
}

switchBtnLeft.addEventListener('click', function(){
	switchLeft();
}, false);

switchBtnRight.addEventListener('click', function(){
	switchRight();
}, false);


//////////



function stopLooping(id) {
  $("#exampleModalLong").removeClass("zoomOut");
  $("#exampleModalLong").addClass("slideInUp");
  $('#carousel-example').carousel('pause');  
  $('#carousel-example').css("z-index", 1050);
  $("#"+id).addClass("active");
  $("#"+id).addClass("arrow-up");
  /*var st = $(this).scrollTop();
  var wh = $(document).height();
  var perc = (st*100)/wh
*/
  $(document).scrollTop(280);
}

function startLooping() {
  $("#exampleModalLong").removeClass("slideInUp");
  $("#exampleModalLong").addClass("zoomOut");

  $('#carousel-example').carousel('cycle');
  $('#carousel-example').css("z-index", 0);
  var anchorid= $("#modeldataid").val();
  $("#"+anchorid).removeClass("active"); 
  $("#"+anchorid).removeClass("arrow-up");

}

$(document).on("click", ".thumb_anchor", function () {
  var anchorid = $(this).attr('id');
  $(".modal-body #modeldataid").val( anchorid );

});


$('ul li').click(function() {
  $('.current').removeClass('current');
  $(this).addClass('current');
});



//////////////////////////////////////////////
// window.onload = function() {
//   document.getElementById('subOverlay').className = 'expand';
// };
// function on() {
//   document.getElementById("subOverlay").style.display = "block";
// }
// function on() {
//   document.getElementById("subOverlay").style.display = "block";
//   setTimeout(function(){

//   }, 2000);
// }

// function off() {
//   document.getElementById("subOverlay").style.display = "none";
// }











