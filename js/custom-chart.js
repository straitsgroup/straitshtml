


    ///// chart js
var barChartData = {
    labels: [
      "2018",
      "2019",
      "2020",
      "2021",
      "2022",
      "2023",
      "2024",
      "2025"
    ],
    datasets: [
      {
        label: "Screen Printing",
        backgroundColor: "#024959",
        borderColor: "#024959",
        borderWidth: 1,
        data: [.8, .8, .9, .9,.7, .8, 1, .8]
      },
      {
        label: "DTG Printing",
        backgroundColor: "#0A84A1",
        borderColor: "#0A84A1",
        borderWidth: 1,
        data: [.4, .7, .3, .6,.5,.7,.4,.6]
      },
      {
        label: "HTP Techniques",
        backgroundColor: "#49C3E0",
        borderColor: "#49C3E0",
        borderWidth: 1,
        data: [.5,.6,.7,.3,.5,.4,.7,.5]
      },
      {
        label: "Others",
        backgroundColor: "#A2A2A2",
        borderColor: "#A2A2A2",
        borderWidth: 1,
        data: [.3,.4,.2,.1,.4,.3,.6,.2]
      }
     
    ]
  };
  
  var chartOptions = {
    responsive: true,
    legend: {
      position: "bottom",
    },

    // title: {
    //   display: true,
    //   text: "Straits"
    // },

    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
  
  window.onload = function() {
    var ctx = document.getElementById("barChart").getContext("2d");
    window.myBar = new Chart(ctx, {
      type: "bar",
      data: barChartData,
      options: chartOptions
    });
  };

  /////////////////////




  // Company Profiles Charts

var barChartData = {
  
  labels: [
    "2018",
    "2019"
  ],

  datasets: [
    {
      label: "Screen Printing",
      backgroundColor: "#024959",
      borderColor: "#024959",
      borderWidth: 1,
      data: [.8]
    },
    {
      label: "DTG Printing",
      backgroundColor: "#0A84A1",
      borderColor: "#0A84A1",
      borderWidth: 1,
      data: [.4]
    },
  ]
};

var chartOptions = {
  responsive: true,
  legend: {
    position: "right",
  },
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      },
      gridLines: {
        color: "rgba(0, 0, 0, 0)",
    }   
    }],
    xAxes: [{
      barPercentage: .5
  }]
  }
}

window.onload = function() {
  var ctx = document.getElementById("cpBarChart").getContext("2d");
  window.myBar = new Chart(ctx, {
    type: "bar",
    data: barChartData,
    options: chartOptions
  });
};
// ////////Bar chart Cp Page end////////


/////////Doughnut (Pie) Chart Cp page//////
var ctx = document.getElementById("cpDChart");
var cpDChart = new Chart(ctx, {
  type: 'pie',
  data: {
    labels: ['OK', 'WARNING', 'CRITICAL', 'UNKNOWN'],
    datasets: [{
      label: '# of Tomatoes',
      data: [12, 19, 3, 5],
      backgroundColor: [
        '#4374A0',
        '#5089BC',
        '#5B9BD5',
        '#BED1EA'
      ],
      borderColor: [
        '#4374A0',
        '#5089BC',
        '#5B9BD5',
        '#BED1EA'
      ],
      borderWidth: 1,
      fill: false,
      pointRadius: 0
    }
  ]
  },
  options: {
   	cutoutPercentage: 50,
    responsive: false,
    legend: {
      position:"right",
      labels: {
        padding: 40,
        // align:"middle"
      }
    }
  }
});

/////////Company Profiles Charts end////////////



// ////////////////Published reports charts (28-12-2019)/////////////

////Donut Chart

// Doughnut Chart 

// Doughnut Chart 
///////


////////////////////

