// Graph Internal chart



// Line Chart
var ctx1 = document.getElementById("giLine").getContext('2d');
var myChart = new Chart(ctx1, {
    type: 'line',
    data: {
        labels: ["Tokyo",	"Mumbai",	"Mexico City",	"Shanghai",	"Sao Paulo",	"New York",	"Karachi","Buenos Aires"],
        datasets: [{
            label: 'Values in millions', 
            data: [500,	50,	2424,	14040,	14141,	4111,	4544,	47], 
            fill: false,
            borderColor: '#1E86CC', 
            backgroundColor: '#1E86CC', 
            borderWidth: 1 
        }]},
    options: {
    responsive: true, 
    maintainAspectRatio: false,  
    }
});
//


// Pie Chart
// Doughnut Chart 
$(document).ready(function(){
	var options = {
		// legend: false,
		responsive: false
	};
	new Chart($("#canvas1"), {
		type: 'doughnut',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
		labels: [
			"blueberry",
			"grape",
			"apple",
			"pineapple"
		],
		datasets: [{
		data: [15, 20, 30, 10],
		backgroundColor: [
			"#2196F3",
			"#48AEFF",
			"#5899CC",
			"#75A9DD"
		],
		hoverBackgroundColor: [
			"#75A9DD",
			"#5899CC",
			"#48AEFF",
			"#2196F3"
		]
		}]
	},
        options: { responsive: false,
            legend: {
                position:"bottom",
            }
            ,
         

        }
	});           
});
// Doughnut Chart 
//


// Bar Chart
var ctx = document.getElementById("barChart").getContext('2d');
var barChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sst", "Sun"],
    datasets: [{
      label: 'data-1',
      data: [12, 19, 3, 17, 28, 24, 7],
      backgroundColor: "#002856"
    }, {
      label: 'data-2',
      data: [30, 29, 5, 5, 20, 3, 10],
      backgroundColor: "#002856"
    }]
  }
});
//


// Graph Internal chart end


